#!/bin/bash
path=$(pwd);
sudo touch $path/settings_for_net;
echo "IP Address - ";IP=192.168.1.50;
echo "netmask (ввиде /24) - "; NETMASK=24;
echo "Gateway -"; GATEWAY=192.168.1.1;
sudo tee  $path/settings_for_net > /dev/null <<EOF
network:
  version: 2
  ethernets:
      enp0s3:
          dhcp4: no
          addresses: [$IP/$NETMASK]
          gateway4: $GATEWAY
          nameservers:
              addresses: [8.8.8.8, 8.8.4.4]
EOF
sudo tee  /etc/netplan/00-installer-config.yaml < $path/settings_for_net;
sudo netplan apply;
sudo touch /etc/apt/sources.list.d/debian.list;
echo "Записываем репозитории дебиан";
sudo tee  /etc/apt/sources.list.d/debian.list > /dev/null <<EOF
deb http://mirror.yandex.ru/debian buster main
deb-src http://mirror.yandex.ru/debian buster main
deb http://mirror.yandex.ru/debian buster-updates main
deb-src http://mirror.yandex.ru/debian buster-updates main
deb https://mirror.yandex.ru/debian-security buster/updates main
deb-src https://mirror.yandex.ru/debian-security buster/updates main
EOF
sudo tee  /etc/apt/trusted.gpg.d/debian.gpg < ./temp/trusted_with_deb.gpg
